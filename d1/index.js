console.log("hello");

// ARTTHMETIC OPERATORS
let x = 1397;
let y = 7831;

// additional operator
let sum = x+y;
console.log("result: " + sum);

// subtraction operator
let difference = x-y;
console.log("result: " + difference);


// multiplication operator
let product = x*y;
console.log("result: " + product);


// division operator
let quotient = x/y;
console.log("result: " + quotient);

// modulo operator
let remainder = y%x;
console.log("result: " + remainder);


// ASSIGNMENT OPERATOR
// basic assignment operator (=)
// the assignment operator adds the value of the right operand to a variable and assigns the result to the variable
let assignmentNumber = 8;

// addition assignment operator
assignmentNumber += 2;
console.log("result of the addition assignment operator: " + assignmentNumber);

assignmentNumber -= 2;
console.log("result of the subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("result of the multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("result of the division assignment operator: " + assignmentNumber);


// multiple operators and parentheses
/*
	When multiple operators are applied in a single statemetn, it follows the PEMDAS rule
*/

let mdas = 1+2-3*4/5;
console.log("result of mdas operation: "+mdas);

// the order of operations can be changed by adding parenthesis to the logic

// it computes the modulo before the PEMDAS


// INCREMENT AND DECREMENT
// pre increment
// operators that add or subtract  values by 1 aand resigns the value of variable where the increment or decrement was applied to

let z = 1;

let preIncrement = ++z;
console.log(preIncrement); // 2
console.log(z); // 2

// post increment

// let postIncrement = z++;
// console.log("result of the post-increment: "+postIncrement); //1
// console.log("result of the postIncrement: "+z); //2

/*
	pre-increment - add 1st value before reading the value
	post-increment - reads value first before adding 1
*/

let a = 2;
// // pre-decrement
// let preDecrement = --a;
// console.log("result for pre-decrement: "+preDecrement) //1
// console.log("result for pre-decrement: "+a) //1




// type coercion / concatenate

/*
	type coercion is the automatic or implicit conversion of values from one data type to another
*/

let numA = "10";
let numB = 12;

/*
	adding or concatenating a string and a number will result as a string
*/

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion); // string

// force coercion

let coercion1 = numA - numB;
console.log(coercion1); //-2
console.log(typeof coercion1); // number

// non-coercion
let numC = 16;
let numD = 14;
let nonCoecion = numC + numD;
console.log(nonCoecion);
console.log(typeof nonCoecion);

// addition of number and boolean
// true = 1; false = 0;
let numE = true + 1;
console.log(numE);
console.log(typeof numE);

// COMPARISON OPERATOR
let juan = "juan";

// equality operator 
// checks whether the operands are eaual or have the same content
// it attempts to convert and compare operands of different data
// it returns a boolean value

console.log(1 == 1); // true
console.log(1 == 2); // false 
console.log(1 == "1"); // true
console.log(1 == true); // true
console.log('true' == true); // false
console.log(juan == "juan"); // true

// inequality
/*
converts wheter the operands are not equal or have diff content 
attepts to convert and compare operands of diff data types
*/


console.log(1 != 1); // f
console.log(1 != 2); // t 
console.log(1 != "1"); // f
console.log(1 != true); // f
console.log('juan' != "juan"); // f
console.log("juan" != juan); // f


/*
	strict equality operator (===)
	checks whether the operands are qual or have the same content
	also compares the data types of values.
*/

console.log(1 === 1); // t
console.log(1 === 2); // f 
console.log(1 === "1"); // f
console.log(1 === true); // f
console.log("juan" === "juan"); // t
console.log(juan === "juan"); // t
console.log('true' === true); // f

// strict inequality operator !==
// checks wheteher the operands are not equal or have the same content. also compares the data types of 2 values

console.log(1 !== 1); // f
console.log(1 !== 2); //  t
console.log(1 !== "1"); // t
console.log(1 !== true); // t
console.log("juan" !== "juan"); // f
console.log(juan !== "juan"); // f


// relational operator
// return a boolean value
let j=50;
let k=65;

// greater than operator (>)
let isGreaterThan = j > k;
console.log(isGreaterThan); //f

let isGTorEqual = j>=k;
console.log(isGTorEqual); //f

let isLTorEqual = j <= k;
console.log(isLTorEqual); //t

let numStr = "30";
console.log(j > numStr);

let str = "thirty";
// 50 is greater than NaN 
console.log(j > str);


// LOGICAL OPERATOR
let isLegalAge = true;
let isRegistered = false;

// Logical AND operator (&&)
// returnds true if all operands are true
// true && true = true
// T && F = F

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet); //f

// locical OR operator (||) double pipe
let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet); //t

// logical NOT operator (!)
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsMet); //t